#+TITLE: Theories
* Cell theory
- All living things are made of cells
- The cell is the most basic unit of life
- All cells come from other living cells
* Scientists of Cell Theory and Microscopes and Germ Theory
** Leeuwenhoek
 - Built a microscope
** Hooke
 - Saw animalcules
** Redi
 - Meat jars
 - Disproved spontaneous generation
 - Used airflow to show it wasn't spontaneous
** Schleiden
 - All plants are made of cells
** Schwann
 - All living things are made of cells
** Virchow
- All cells come from other living cells
  - Omnis cellula e cellula
** Swan-necked Pasteur
- Experiment
  - Used a bunsen burner
  - Used a swan neck flask
    - This didn't allow bacteria and dust to get in
    - Created clean/forever usable broth
* Endosymbiotic Theory
- *Eukaryotes* began as a symbiotic r4elationship between one prokarote *IN*-side another prokaryote
- Mitochondria and chloroplasts are similar in size to a prokaryote
- They have two membranes
  - As if they entered as food surrounded by a vessicle
- They have DNA + ribosomes
- They can divide independently of the cell
- We both use: DNA+20 amino acids of the same isomer
  

