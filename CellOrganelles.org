* Vacuoles
- In *plant AND animal cells*
- Larger in plant cells
- Store water
- Pigments
- Toxic substances
  - Helps to deter predation in plant cells
- Contain enzymes
- As it grows, puts pressure on the Cell Wall
- Maintains turgor pressure
  - Reason why plants wilt/shrivel up
  - Keeps pressure on the cell wall
  - If the vacuole gets too small, cell membrane peels away from cell wall
* Peroxisomes
- Break down peroxides (like H2O2)
- Almost all eukaroytes need this cell
* Nucleus
- Where the [[DNA]] is stored and used to controller cellular acitivies
- Where the replication of the [[DNA]] takes place
** Nuclear envelope
- Outter and inner nuclear membrane
- Each layer is a phospholipid *bi*layer
- mRNA has to leave via a special set of proteins
** Nucleolus
- Produces [[Ribosomes]]
** DNA
- Loosely organized as [[Chromatin]]
*** Chromatin
- DNA + Protein
** Nuclear pores
- Protein complexs
- Only lets small things in
- Other things need a special chemical entry sequence
** Nuceoplasm
- Fancy name for liquid contents inside the nucleus
** Nuclear Lamina
- Special mesh of protein fibers
- Keeps [[DNA]] usable
- Holds the [[Chromatin]] to the [[Nuclear envelope]]
* Cell Envelope (Bacteria)
Includes plasma membrane, cell wall, and glycoalyx
- Plasma Membrane: Phospholipid bilayer with embedded proteins
- Cell Wall: Maintains the shape of the cell
- Glycocalyx: Layer of polysaccharides (sugars) that lie outside the cell wall
  - When layer is well organized and not easily washed off it is called a *capsule*
  - Helps to aid against drying out and resisting against a host's immune system
  - Helps bacteria to attach to near-any surface
* Cytoplasm
- Semifluid solution encased by a plasma membrane. (has some water in it)
* Ribosomes
- Travel out through [[Nuclear pores]] into the [[Cytoplasm]]
- Made out of 2 RNA subunits
  - mRNA and tRNA
- Called rRNA
- Synthesises proteins by linking amino acids
* Endoplasmic Reticulum (E.R)
- Shuttles important proteins
- Shuttle things from [[Nucleus]] --> [[Golgi Body]] --> Membrane
- Instructions come from the [[Nucleus]]
- Lumen: Interior space of the ER
- Folds allow for lots of Surface Area
** Rough E.R
- Called rough due to lots of [[Ribosomes]]
  - Which synthesize proteins
- Proteins leave through vesicle
- Can have many things done to them
- Proteins are tagged using carbohydrates
** Smooth E.R
- More tubey
- Modifies proteins here
- Hydrolyzes Glycegen
- Synthesis lipids
- Does *not* make protein
* Golgi Body
- Chemical modifications to proteins
- Sorting
- Packaging
- Can have proteins inside vesicle membrane, which can cause it to be part of the cell membrane
** Faces
- Cis Face: Near the nucleus, where vesicles enter
- Trans Face: Where vesicles exit the Golgi
* Lysosome
- In *animal* cells
- Buds of the Trans Face of the [[Golgi Body]]
- Does *not* exit the cell
- Eats food vessicle thingies?
- Removes invaders and damaged organelles
- Full of hydrolyzing enzymes
  - Which makes it go through hydrolysis and break shit up
* Mitochondria
- Glucose is broken in the cytoplasm
  - Creates pyruvate
    - you know the rest boi (Carbon dioxide and water)
    - and creates ATP from food energy
- Singular is Mitochondrion
- Two Membranes
  - Outer is smooth
  - Inner is folded
    - Folds are called *Cristae*
    - Space inside here is called the Mitochondrial Matrix
      - It includes ribosomes and mitochondrial DNA
* Chloroplast
- Found only in plants/coral/licehn
- Contain their own DNA
- Two membranes and membrane section thingies (thylakoid)
